import { Sequenza } from './sequenza'

// Enable references to Sequenza when loaded via traditional script tag
window.Sequenza = Sequenza

// Enable references to Sequenza when imported as an ES6 module
export { Sequenza }
