import * as d3axis from 'd3-axis'
import * as d3scale from 'd3-scale'

import { Layout } from '../layouts/layouts'

function chromview2 (data) {
  var x, y
  var graph = this
  graph._layout = Layout.getLayout(this.config, graph)
  x = d3scale.scaleLinear()
    .domain(graph.config.pos_edges)
    .range([0, graph._layout._width])
  y = d3scale.scaleLinear()
    .domain(graph.config.ratio_edges)
    .range([0, graph._layout._height])
  console.log('da da da2')
}

export { chromview2 }
