/**
 * @fileoveriew Methods for initialization
 */

import { chromview } from './chromview'
import { chromview2 } from './chromview2'

export {
  chromview, chromview2
}
