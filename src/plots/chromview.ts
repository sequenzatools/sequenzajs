import * as d3axis from 'd3-axis'
import * as d3scale from 'd3-scale'
import * as d3color from 'd3-color'

function chromview (data, config, graph, svg) {

  const x = d3scale.scaleLinear()
    .domain(graph.config.pos_edges)
    .range([config.padding, graph._layout._width - config.padding])
  const yRatio = d3scale.scaleLinear()
    .domain(graph.config.ratio_edges)
    .range([config.padding, graph._layout._plotheight - config.padding])
  const yBaf = d3scale.scaleLinear()
    .domain(graph.config.baf_edges)
    .range([config.padding, graph._layout._plotheight - config.padding])
  const yVaf = d3scale.scaleLinear()
    .domain(graph.config.vaf_edges)
    .range([config.padding, graph._layout._plotheight - config.padding])

  const posAxis = d3axis.axisBottom(x)
    .ticks(10)
    .tickFormat(function (d, i) {
      return (d / 1000000)
    })

  const pos0Axis = d3axis.axisBottom(x)
    .ticks([])

  const vafplot = svg.append('g').attr('class', 'vafplot')
  const bafplot = svg.append('g').attr('class', 'bafplot')
    .attr('transform', 'translate(0,' + graph._layout._plotheight + ')')
  const ratioplot = svg.append('g').attr('class', 'ratioplot')
    .attr('transform', 'translate(0,' + 2 * graph._layout._plotheight + ')')

  const ratioAxis = d3axis.axisLeft(yRatio)
    .ticks(5)
  const bafAxis = d3axis.axisLeft(yBaf)
    .ticks(5)
  const vafAxis = d3axis.axisLeft(yVaf)
    .ticks(5)

  ratioplot.append('g')
    .attr('class', 'axis y-axis')
    .attr('transform', 'translate(' + config.padding + ',0)')
    .call(ratioAxis)

  bafplot.append('g')
    .attr('class', 'axis y-axis')
    .attr('transform', 'translate(' + config.padding + ',0)')
    .call(bafAxis)

  vafplot.append('g')
    .attr('class', 'axis y-axis')
    .attr('transform', 'translate(' + config.padding + ',0)')
    .call(vafAxis)

  ratioplot.append('g')
    .attr('class', 'axis x-axis')
    .attr('transform', 'translate(0,' +
      (graph._layout._plotheight - config.padding) + ')')
    .call(posAxis)

  bafplot.append('g')
    .attr('class', 'axis x-axis')
    .attr('transform', 'translate(0,' +
      (graph._layout._plotheight - config.padding) + ')')
    .call(pos0Axis)

  vafplot.append('g')
    .attr('class', 'axis x-axis')
    .attr('transform', 'translate(0,' +
      (graph._layout._plotheight - config.padding) + ')')
    .call(pos0Axis)

  // Add labels in the X-axis
  ratioplot.append('text')
    .attr('text-anchor', 'middle')
    .attr('transform', 'translate(' +
      (graph._layout._width / 2) + ',' +
      (graph._layout._plotheight + graph._layout._margins.bottom) + ')')
    .text('Position (Mb)')
  // Add labels for the 3 Y-axis
  ratioplot.append('text')
    .attr('text-anchor', 'middle')
    .attr('transform', 'translate(' + -graph._layout._margins.left / 3 * 2 + ',' +
      graph._layout._plotheight / 2 + ')rotate(-90)')
    .text('Depth ratio')
  bafplot.append('text')
    .attr('text-anchor', 'middle')
    .attr('transform', 'translate(' + -graph._layout._margins.left / 3 * 2 + ',' +
      graph._layout._plotheight / 2 + ')rotate(-90)')
    .text('Minor allele frequency')
  vafplot.append('text')
    .attr('text-anchor', 'middle')
    .attr('transform', 'translate(' + -graph._layout._margins.left / 3 * 2 + ',' +
      graph._layout._plotheight / 2 + ')rotate(-90)')
    .text('Mutation Frequency')
  // Draw windows for the ratio
  ratioplot.selectAll('bar')
    .data(data['ratio.windows'])
    .enter().append('polygon')
    .style('stroke', 'none')
    .style('fill', 'lightblue')
    .style('opacity', 0.8)
    .attr('points', function (d) {
      return getPointsWindows(d, x, yRatio, graph.config.ratio_edges[0])
    })
  // Draw middle value for the ratio
  ratioplot.selectAll('bar')
    .data(data['ratio.windows'])
    .enter().append('rect')
    .style('stroke', 'black')
    .style('fill', 'black')
    .style('opacity', 0.5)
    .attr('x', function (d) { return x(d.start) })
    .attr('y', function (d) {
      return getYMiddle(d, yRatio, graph.config.ratio_edges[0])
    })
    .attr('width', function (d) { return x(d.end - d.start) })
    .attr('height', function (d) {
      return getBarHeight(d, yRatio, graph.config.ratio_edges[0])
    })
  // Draw polygons for BAF
  bafplot.selectAll('bar')
    .data(data['baf.windows'])
    .enter().append('polygon')
    .style('stroke', 'none')
    .style('fill', 'lightblue')
    .style('opacity', 0.8)
    .attr('points', function (d) {
      return getPointsWindows(d, x, yBaf, graph.config.baf_edges[0])
    })
  // Draw middle value for the ratio
  bafplot.selectAll('bar')
    .data(data['baf.windows'])
    .enter().append('rect')
    .style('stroke', 'black')
    .style('fill', 'black')
    .style('opacity', 0.5)
    .attr('x', function (d) { return x(d.start) })
    .attr('y', function (d) {
      return getYMiddle(d, yBaf, graph.config.baf_edges[0])
    })
    .attr('width', function (d) { return x(d.end - d.start) })
    .attr('height', function (d) {
      return getBarHeight(d, yBaf, graph.config.baf_edges[0])
    })
  // Draw points for mutations
  vafplot.selectAll('dot')
    .data(data['mut.tab'])
    .enter().append('circle')
    .attr('r', 2.5)
    .style('fill', function (d) { return getColors(d) })
    .attr('cx', function (d) { return x(d.position) })
    .attr('cy', function (d) { return yVaf(d.F) })
  // Add mutations legend
  const legend = vafplot.append('g')
    .attr('class', 'legend')
    .attr('height', 100)
    .attr('width', 100)
    .attr('transform', 'translate(-20,50)')

  const legendData = [
    { 'mutation': 'A>C', 'text': 'A>C, T>G' },
    { 'mutation': 'A>G', 'text': 'A>G, T>C' },
    { 'mutation': 'A>T', 'text': 'A>T, T>A' },
    { 'mutation': 'C>A', 'text': 'C>A, G>T' },
    { 'mutation': 'C>G', 'text': 'C>G, G>C' },
    { 'mutation': 'C>T', 'text': 'C>T, G>A' }
  ]

  legend.selectAll('dot')
    .data(legendData)
    .enter().append('circle')
    .attr('r', 3)
    .style('fill', function (d) { return getColors(d) })
    .attr('cx', graph._layout._width +
      config.padding + 25)
    .attr('cy', function (d, i):number { return (i - 1 + 0.25) * 20 })
    .style('fill', function (d) { return getColors(d) })

  legend.selectAll('text')
    .data(legendData)
    .enter()
    .append('text')
    .attr('x', graph._layout._width + 40)
    .attr('y', function (d, i):number { return (i - 1) * 20 + 10 })
    .text(function (d):string {
      return d.text
    })
  // Add segments
  bafplot.selectAll('bar')
    .data(data['segments'])
    .enter().append('polygon')
    .style('stroke', 'red')
    .attr('id', 'segments')
    .attr('points', function (d) {
      let x0:number, x1:number, y0:number
      x0 = d['start.pos']
      x1 = d['end.pos']
      y0 = d['Bf']
      const points = [[x(x0), yBaf(y0)].join(','), [x(x0), yBaf(y0)].join(','),
        [x(x1), yBaf(y0)].join(','), [x(x1), yBaf(y0)].join(',')].join(' ')
      return points
    })
  ratioplot.selectAll('bar')
    .data(data['segments'])
    .enter().append('polygon')
    .style('stroke', 'red')
    .attr('id', 'segments')
    .attr('points', function (d) {
      let x0, x1, y0
      x0 = d['start.pos']
      x1 = d['end.pos']
      y0 = d['depth.ratio']
      const points = [[x(x0), yRatio(y0)].join(','), [x(x0), yRatio(y0)].join(','),
        [x(x1), yRatio(y0)].join(','), [x(x1), yRatio(y0)].join(',')].join(' ')
      return points
    })
  // graph.ratioplot = ratioplot
  // graph.bafplot = bafplot
  // graph.vafplot = vafplot
}

function getColors (d) {
  const colorsConv = {
    'NA': d3color.rgb(0, 0, 0, 0),
    'A>C': d3color.rgb(0, 178, 238, 0.4),
    'T>G': d3color.rgb(0, 178, 238, 0.4),
    'A>G': d3color.rgb(255, 64, 64, 0.4),
    'T>C': d3color.rgb(255, 64, 64, 0.4),
    'A>T': d3color.rgb(34, 139, 34, 0.4),
    'T>A': d3color.rgb(34, 139, 34, 0.4),
    'C>A': d3color.rgb(139, 90, 0, 0.4),
    'G>T': d3color.rgb(139, 90, 0, 0.4),
    'C>G': d3color.rgb(127, 0, 255, 0.4),
    'G>C': d3color.rgb(127, 0, 255, 0.4),
    'C>T': d3color.rgb(255, 215, 0, 0.4),
    'G>A': d3color.rgb(255, 215, 0, 0.4)
  }
  return colorsConv[d.mutation]
}

function getBarHeight (d, y, m): number {
  const h = d.q1 - d.q0
  if (isNaN(h)) {
    return 0
  } else {
    if (d.mean <= m) {
      return 1 / 3
    } else {
      return 0
    }
  }
}

function getYMiddle (d, y, m): number {
  const h = d.q1 - d.q0
  if (isNaN(h)) {
    return 0
  } else {
    if (d.mean <= m) {
      return y(d.mean)
    } else {
      return 0
    }
  }
}

function getPointsWindows (d, x, y, m) {
  let x0:number, x1:number, y0:number, y2:number
  if (isNaN(d.q1) || isNaN(d.q0) || isNaN(d.mean)) {
    y0 = 0
    y2 = 0
  } else {
    if (d.q1 <= m) {
      y0 = d.q0
      y2 = d.q1
    } else {
      if (d.q0 <= m) {
        y0 = d.q0
        y2 = m
      } else {
        y0 = 0
        y2 = 0
      }
    }
  }
  x0 = d.start
  x1 = d.end
  const points = [[x(x0), y(y0)].join(','), [x(x0), y(y2)].join(','),
    [x(x1), y(y0)].join(','), [x(x1), y(y2)].join(',')].join(' ')
  return points
}

export { chromview }
