/**
 * @fileoverview Core module of sequenza.js, links all other modules
 * This file defines the Sequenza class, its constructor method, and its
 * static methods.  All instance methods are defined in other modules.
 *
 */

import * as d3selection from 'd3-selection'
import * as d3fetch from 'd3-fetch'
import * as d3brush from 'd3-brush'
import * as d3dispatch from 'd3-dispatch'
import { scaleLinear } from 'd3-scale'
import { max } from 'd3-array'

import { version } from './version'

import {
  configure, init
} from './init/init'

let d3 = Object.assign({}, d3selection, d3fetch, d3brush, d3dispatch)
d3.scaleLinear = scaleLinear
d3.max = max

export class Sequenza {
  constructor (data, config) {
    // Functions from init.js
    this.configure = configure
    this.init = init
    this.configure(data, config)
  }
  /**
   * Get the current version of Sequenza.js
   */
  static get version () {
    return version
  }

  /**
  * Enable use of D3 in client apps, via "d3 = Sequenza.d3"
  */
  static get d3 () {
    return d3
  }
}
