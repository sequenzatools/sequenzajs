
export class Layout {
  constructor (config, graph) {
    this._margins = config.margins
    this._width = config.width - this._margins.left - this._margins.right
    this._height = config.height - this._margins.top - this._margins.bottom
  }

  // Factory method
  static getLayout (config, graph) {
    if (config.plottype === 'chromview') {
      return new ChromViewLayout(config, graph)
    } else if (config.plottype === 'chromview2') {
      return new ChromViewLayout2(config, graph)
    }
  }
}

export class ChromViewLayout extends Layout {
  constructor (config, graph) {
    super(config, graph)
    // 3 panels equal size, common x-axis
    // 3 different y-axis
    // Reduce y axisi to 1 / 3
    this._plotheight = this._height / 3
  }
}

export class ChromViewLayout2 extends Layout {
  constructor (config, graph) {
    super(config, graph)
    console.log('chromview2')
  }
}
