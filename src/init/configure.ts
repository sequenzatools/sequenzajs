/**
 * High-level helper method for Sequenza constructor.
 *
 * @param config Configuration object.  Enables setting Sequenza properties.
 */

import { max } from 'd3-array'

function configure (data, config) {
  // Clone the config object, to allow multiple instantiations
  // without picking up prior ideogram's settings
  this.config = JSON.parse(JSON.stringify(config))
  this.config.pos_edges = [0, max(data['ratio.windows'],
    function (d) { return d.end })]
  if (!this.config.debug) {
    this.config.debug = false
  }

  if (!this.config.container) {
    this.config.container = 'body'
  }

  if (!this.config.padding) {
    this.config.padding = 5
  }

  if (!this.config.ratio_max) {
    this.config.ratio_max = 2.5
  }

  this.config.ratio_edges = [this.config.ratio_max, 0]
  this.config.baf_edges = [0.5, 0]
  this.config.vaf_edges = [1, 0]

  if (!this.config.plottype) {
    this.config.plottype = 'chromview'
  }

  if (!this.config.width) {
    this.config.width = 700
  }

  if (!this.config.height) {
    this.config.height = 600
  }

  if (!this.config.margins) {
    this.config.margins = { 'top': 20, 'right': 80, 'bottom': 40, 'left': 40 }
  }

  this.init(data)
}

export { configure }
