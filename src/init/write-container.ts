
/**
 * Writes the HTML elements that contain this sequenza instance.
 */
import * as d3selection from 'd3-selection'

import { Layout } from '../layouts/layouts'

function writeContainer (graph) {
  graph._layout = Layout.getLayout(graph.config, graph)
  let svg = d3selection.select(graph.config.container)
    .append('div')
    .attr('id', '_sequenzaOuterWrap')
    .append('div')
    .attr('id', '_sequenzaInnerWrap')
    .append('svg')
    .attr('id', '_sequenza')
    .attr('width', graph._layout._width + graph._layout._margins.left +
      graph._layout._margins.right)
    .attr('height', graph._layout._height + graph._layout._margins.top +
      graph._layout._margins.bottom)
    .append('g')
    .attr('transform',
      'translate(' + graph._layout._margins.left +
      ',' + graph._layout._margins.top + ')')
  return svg
}

export { writeContainer }
