/**
 * @fileoveriew Methods for initialization
 */
import { chromview } from '../plots/plots'

import { configure } from './configure'
import { writeContainer } from './write-container'

/**
 * Initializes a graph.
 */
function init (data) {
  let graph = this
  let svg = writeContainer(graph)
  chromview(data, graph.config, graph, svg)
}

export {
  configure, init, writeContainer
}
