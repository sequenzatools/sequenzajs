const { FuseBox, QuantumPlugin, WebIndexPlugin} = require("fuse-box");
const { src, task, watch, context, fuse } = require("fuse-box/sparky");

context(class {
    getConfig() {
        return FuseBox.init({
            homeDir: "src",
            output: "dist/$name.js",
            hash: this.isProduction,
            target: "browser@es6",
            useTypescriptCompiler: true,
            plugins: [
                WebIndexPlugin({
                    template: "examples/index.html"
                }),
                this.isProduction && QuantumPlugin({
                    uglify: true,
                    bakeApiIntoBundle: "sequenzajs"
                })
            ]
        })
    }
    createBundle(fuse) {
        const app = fuse.bundle("sequenzajs");
        if (!this.isProduction) {
            app.watch()
            app.hmr()
        }
        app.instructions(">index.ts");
        return app;
    }
});

task("clean", () => src("dist").clean("dist").exec() )

task("copy:test-data", () =>
    src("data/chr1.json").dest("dist/$name").exec())
task("copy:test-page", () =>
    src("examples/index.html").dest("dist/$name").exec())


task("default", ["clean", "copy:test-data"], async context => {
    const fuse = context.getConfig();
    fuse.dev();
    context.createBundle(fuse);
    await fuse.run();
});

task("dist", ["clean"], async context => {
    context.isProduction = true;
    const fuse = context.getConfig();
    //fuse.dev(); // remove it later
    context.createBundle(fuse);
    await fuse.run();
});

